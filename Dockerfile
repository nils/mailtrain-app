FROM cloudron/base:0.10.0
MAINTAINER Johannes Zellner <support@cloudron.io>

ENV PATH /usr/local/node-6.9.5/bin:$PATH

RUN mkdir -p /app/code
WORKDIR /app/code

RUN curl -L https://github.com/andris9/mailtrain/archive/dcf83309294eee6cdaefd312b8b8c1c92d7ab71f.tar.gz | tar -xz --strip-components 1 -f -
COPY patches/fix-collation.patch /app/code/fix-collation.patch
RUN patch -p1 -d /app/code < /app/code/fix-collation.patch
RUN npm install --production && npm install passport-ldapjs tomlify-j0.4
COPY toml-override.js /app/code/toml-override.js

# Certain directories needs to be backed up (see #13)
RUN rm -rf /app/code/public/mosaico/uploads && ln -s /app/data/public/mosaico/uploads /app/code/public/mosaico/uploads && \
    rm -rf /app/code/public/grapejs/uploads && ln -s /app/data/public/grapejs/uploads /app/code/public/grapejs/uploads && \
    rm -rf /app/code/protected/reports && ln -s /app/data/protected/reports /app/code/protected/reports

RUN ln -s /run/mailtrain/production.toml /app/code/config/production.toml

COPY cloudron-setup.js production.toml.template start.sh /app/code/
COPY index.hbs layout.hbs /app/code/views/

CMD [ "/app/code/start.sh" ]
