#!/bin/bash

set -eu

export NODE_ENV=production

# https://github.com/Mailtrain-org/mailtrain/issues/250
mkdir -p /run/mailtrain /app/data/public/mosaico/uploads /app/data/public/grapejs/uploads /app/data/protected/reports

sed -e "s!##MYSQL_HOST##!${MYSQL_HOST}!" \
    -e "s!##MYSQL_PORT##!${MYSQL_PORT}!" \
    -e "s!##MYSQL_USERNAME##!${MYSQL_USERNAME}!" \
    -e "s!##MYSQL_PASSWORD##!${MYSQL_PASSWORD}!" \
    -e "s!##MYSQL_DATABASE##!${MYSQL_DATABASE}!" \
    -e "s!##REDIS_HOST##!${REDIS_HOST}!" \
    -e "s!##REDIS_PORT##!${REDIS_PORT}!" \
    -e "s!##REDIS_PASSWORD##!${REDIS_PASSWORD}!" \
    -e "s!##LDAP_SERVER##!${LDAP_SERVER}!" \
    -e "s!##LDAP_PORT##!${LDAP_PORT}!" \
    -e "s!##LDAP_USERS_BASE_DN##!${LDAP_USERS_BASE_DN}!" \
    -e "s!##WEBADMIN_ORIGIN##!${WEBADMIN_ORIGIN}!" \
    /app/code/production.toml.template > /run/mailtrain/production.toml

if [[ ! -f /app/data/production.toml ]]; then
    echo "# Add additional customizations in this file" > /app/data/production.toml
fi

chown -R cloudron:cloudron /app/data /run/mailtrain

# merge user toml file (toml does not allow key re-declaration)
/usr/local/bin/gosu cloudron:cloudron node /app/code/toml-override.js /run/mailtrain/production.toml /app/data/production.toml

cd /app/code

/usr/local/bin/gosu cloudron:cloudron node cloudron-setup.js
exec /usr/local/bin/gosu cloudron:cloudron npm start

