#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var assert = require('assert'),
    execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);
    var firefox = require('selenium-webdriver/chrome');
    var server, browser = new firefox.Driver();
    var LOCATION = 'test';
    var TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var email, token;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT)
        .then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function login(done) {
        browser.get('https://' + app.fqdn + '/users/login').then(function () {
            return waitForElement(by.id('usernameMain'));
        }).then(function () {
            return browser.findElement(by.id('usernameMain')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.id('passwordMain')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.tagName('form')).submit();
        }).then(function () {
            return waitForElement(by.xpath('//*[text()[contains(., "' + username + '")]]'));
        }).then(function () {
            done();
        });
    }

    function logout(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return waitForElement(by.xpath('//*[text()[contains(., "' + username + '")]]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[text()[contains(., "' + username + '")]]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//*[text()[contains(., "Log out")]]'));
        }).then(function () {
            return browser.findElement(by.xpath('//*[text()[contains(., "Log out")]]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//*[text()[contains(., "Sign in")]]'));
        }).then(function () {
            done();
        });
    }

    function checkSettings(done) {
        browser.get('https://' + app.fqdn + '/settings').then(function () {
            return browser.findElement(by.id('service-url')).getAttribute('value');
        }).then(function (value) {
            expect(value).to.be('https://' + app.fqdn + '/');
            return browser.findElement(by.id('smtp-hostname')).getAttribute('value');
        }).then(function (value) {
            expect(value).to.be('mail');
            done();
        });
    }

    function checkMailerConfig(done) {
        browser.get('https://' + app.fqdn + '/settings');

        var button = browser.findElement(by.id('verify-button'));

        browser.executeScript('arguments[0].scrollIntoView(false)', button).then(function () {
            return button.click();
        }).then(function () {
            console.log('The test will dismiss the alert dialog after 10 seconds');
            return browser.sleep(10000);
        }).then(function () {
            var alert = browser.switchTo().alert();
            alert.getText().then(function (text) {
                expect(text).to.be('Mailer settings verified, ready to send some mail!');

                alert.accept().then(function () { done(); });
            });
        });
    }

    function createTemplate(done) {
        browser.get('https://' + app.fqdn + '/templates/create').then(function () {
            return browser.findElement(by.id('template-name')).sendKeys('custommosaico');
        }).then(function () {
            return browser.findElement(by.css('#editor_name>option[value="mosaico"]')).click();
        }).then(function () {
            return browser.findElement(by.xpath('//button[@type="submit"]')).click();
        }).then(function () {
            done();
        });
    }

    function uploadMosaicoImage(done) {
        var openMosaico;
        var imageGallery;

        browser.get('https://' + app.fqdn + '/templates/edit/1').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[text()="Open Mosaico"]')));
        }).then(function () {
            openMosaico = browser.findElement(by.xpath('//a[text()="Open Mosaico"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', openMosaico);
        }).then(function () {
            return openMosaico.click();
        }).then(function () {
            console.log('sleeping for 10secs for page to load');
            return browser.sleep(10000);
        }).then(function () {
            return browser.switchTo().frame("editor-frame");
        }).then(function () {
            imageGallery = browser.findElement(by.xpath('//label[@title="Show image gallery"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', imageGallery);
        }).then(function () {
            return imageGallery.click();
        }).then(function () {
            return browser.findElement(by.xpath('//input[@class="fileupload"]')).sendKeys(path.resolve(__dirname + '/../logo.png'));
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//img[@title="Drag this image and drop it on any template image placeholder"]')), TEST_TIMEOUT);
        }).then(function () {
            browser.switchTo().defaultContent();
            done();
        });
    }

    function checkGallery(done) {
        var openMosaico;
        browser.get('https://' + app.fqdn + '/templates/edit/1').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[text()="Open Mosaico"]')));
        }).then(function () {
            openMosaico = browser.findElement(by.xpath('//a[text()="Open Mosaico"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', openMosaico);
        }).then(function () {
            return openMosaico.click();
        }).then(function () {
            return browser.sleep(10000);
        }).then(function () {
            return browser.switchTo().frame("editor-frame");
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//label[@title="Show image gallery"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//label[@title="Show image gallery"]')).click();
        }).then(function () {
            return browser.sleep(10000); // let the animation finish
        }).then(function () {
            return browser.findElement(by.xpath('//a[@title="Remote gallery"]')).click();
        }).then(function () {
            return browser.sleep(10000); // let the images load
        }).then(function () {
            var img = browser.findElement(by.xpath('//img[@title="Drag this image and drop it on any template image placeholder"]'));
            return browser.executeScript('return arguments[0].complete && arguments[0].naturalWidth', img);
        }).then(function (imageWidth) {
            assert(imageWidth === 90);
            browser.switchTo().defaultContent();
            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('setup settings correctly', checkSettings);
    it('check mailer config', checkMailerConfig);
    it('can create template', createTemplate);
    it('can upload mosaico image', uploadMosaicoImage);
    it('can check gallery', checkGallery);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart --wait --app ' + app.id);
        done();
    });

    it('can login', login);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login);
    it('can check gallery', checkGallery);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('setup settings correctly', checkSettings);
    it('check mailer config', checkMailerConfig);
    it('can check gallery', checkGallery);

    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --new --wait --appstore-id org.mailtrain.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can login', login);
    it('check mailer config', checkMailerConfig);
    it('can create template', createTemplate);
    it('can upload mosaico image', uploadMosaicoImage);
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can login', login);
    it('setup settings correctly', checkSettings);
    it('check mailer config', checkMailerConfig);
    it('can check gallery', checkGallery);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

});
