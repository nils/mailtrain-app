#!/usr/bin/env node

'use strict';

var fs = require('fs'),
    toml = require('toml'),
    tomlify = require('tomlify-j0.4');

var target = toml.parse(fs.readFileSync(process.argv[2]));
var source = toml.parse(fs.readFileSync(process.argv[3]));
target = Object.assign(target, source);
fs.writeFileSync(process.argv[2], tomlify.toToml(target, null, 4));

